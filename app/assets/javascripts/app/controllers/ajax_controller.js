app.controller('AjaxCtrl', ['$http','$scope', function($http,$scope){
	$scope.$watch('search', function(val){
		$http({method:"GET", url:"/api/filter", params: {name: val}}).success(function (res) {
			$scope.find = res;
		})
	})
}])