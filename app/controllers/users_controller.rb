class UsersController < ApplicationController
  before_action :get_params, only: :create
  before_action :get_current_user, only: [:update, :create, :edit, :show, :destroy]

  def index
    @users = User.all
    @user_new = User.new
  end

  def new
    @user = User.new
  end

  def show
  end

  def update
    if @user.update(get_params)
      redirect_to user_path(@user.id)
    else
      render 'edit'
    end
  end

  def destroy
    @user.destroy
    redirect_to users_path
  end

  def edit
  end

  def create
    if @user.save
      redirect_to users_path
    else
      render 'new'
    end
  end

  private
    def get_params
      params[:user].permit(:name, :age, :info)
    end
    def get_current_user
      @user = User.find(params[:id])
    end
end
