class User < ActiveRecord::Base
	validates :name, presence: true
	validates :age, numericality: { only_integer: true, greater_than: 18,  less_than: 90}

end
